# Changelog for "auhtorization-service"
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## \[v2.1.3] [r4.24.0] - 2020-06-22

### Fixes
- bug on ApiKey Management (https://support.d4science.org/issues/19487)

