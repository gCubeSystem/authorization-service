package org.gcube.common.authorizationservice;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.gcube.common.authorizationservice.configuration.AuthorizationConfiguration;

@ApplicationPath("/gcube/service/*")
public class AuthorizationService extends  Application {
	
	public static AuthorizationConfiguration configuration;
	
	
	@Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<>();
        classes.add(TokenManager.class);
        classes.add(KeyRetriever.class);
        classes.add(PolicyManager.class);
        classes.add(TokenManager.class);
        classes.add(ApiKeyManager.class);
        return classes;
    }
	
	
		
}