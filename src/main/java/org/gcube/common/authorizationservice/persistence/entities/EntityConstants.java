package org.gcube.common.authorizationservice.persistence.entities;

public class EntityConstants {

	public static final String USER_POLICY="UserPolicy"; 
	public static final String SERVICE_POLICY="ServicePolicy"; 
	
	public static final String USER_AUTHORIZATION="UserInfo"; 
	public static final String SERVICE_AUTHORIZATION="ServiceInfo"; 
	public static final String EXTERNAL_SERVICE_AUTHORIZATION="ExternalServiceInfo"; 
	public static final String CONTAINER_AUTHORIZATION="ContainerServiceInfo"; 
	
}
